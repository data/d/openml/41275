# OpenML dataset: wine-reviews

https://www.openml.org/d/41275

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

130k wine reviews with variety, location, winery, price, and description. Downloaded from Kaggle [https://www.kaggle.com/zynicide/wine-reviews/home] on 29.10.2018. The original data was scraped from the WineEnthusiast homepage [https://www.winemag.com/?s=&drink_type=wine]. The second version of the dataset was used, which was scraped on 22.11.2017. The Kaggle dataset was licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) [https://creativecommons.org/licenses/by-nc-sa/4.0/]. The variable 'points' (the number of points WineEnthusiast rated the wine on a scale of 1-100) was selected as target variable. For a description of all variables, checkout the Kaggle dataset repo. The variable 'region_2' is ignored by default as it contains a large portion of missing values. The variable 'designation' is not used by default, as the number of factor labels is extremely high compared to the number of observations. The dataset further includes the text based variables 'description', 'taster_twitter_handle', and 'title' (ignored by default) which could be used to construct additional features. Special characters in text features have been removed to allow the upload to the platform. The ID variable from the Kaggle version was removed from the dataset. The factor labels of all nominal features had to be changed to integers to prevent a problem which would not allow the upload of nominal features with too many and too long labels.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41275) of an [OpenML dataset](https://www.openml.org/d/41275). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41275/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41275/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41275/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

